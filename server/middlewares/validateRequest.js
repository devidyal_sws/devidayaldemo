var jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {

  // When performing a cross domain request, you will recieve
  // a preflighted request first. This is to check if our the app
  // is safe. 

  // We skip the token outh for [OPTIONS] requests.
  //if(req.method == 'OPTIONS') next();

  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];

  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, require(__basedir + '/helpers/secret')(), function (err, decoded) {
      if (err)
        return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

      // if everything is good, save to request for use in other routes
      req.userId = decoded.uuid;
      next();
    });

  } else {
    res.status(403);
    res.json({
      "status": 403,
      "message": "No token provided"
    });
    return;
  }
};
