// Variables
require('dotenv').config();
let express = require('express');
let path = require('path');
let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');
const helmet = require('helmet');
const cors = require('cors');

global.__basedir = __dirname;
//let { sequelize } = require(__dirname + "/models");
const app = express();

// Initlizers
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());
app.use(cors());


// Routes

app.use("*", [require('./middlewares/validateIp')], require('./routes/common'));
app.use('/auth', require('./routes/auth'));
app.use('/api/v1/', [require('./middlewares/validateRequest')], require('./routes/index'));
app.use('/error', require('./routes/errors'));

app.use(function (req, res, next) {
    res.redirect('/error/404');
    next();
});

app.use(function (err, req, res, next) {
    console.log(err);
    console.log("500 Internal error", err);
    res.redirect('/error/500');
});

// Exporter
module.exports = app;
