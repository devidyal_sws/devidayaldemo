var express = require('express');
var router = express.Router();
const AuthCtrl = require(__basedir + '/controllers/auth');

/*Routes for First Authentication */
router.post('/', AuthCtrl.login);

module.exports = router;