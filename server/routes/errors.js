var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.status(200);
  res.json({
    "status": 200,
    "message": "app is running"
  });
});

router.get('/500', function (req, res, next) {
  res.status(500);
  res.json({
    "status": 500,
    "message": "internal server error"
  });
});

router.get('/404', function (req, res, next) {
  res.status(404);
  res.json({
    "status": 404,
    "message": "page not found"
  });
});

module.exports = router;