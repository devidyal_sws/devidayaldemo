var mongoose = require('mongoose');
var mongoDB = 'mongodb://'+process.env.dbhost+'/'+process.env.database;
var db = mongoose.connect(mongoDB, { useNewUrlParser: true }).then(() => {
    console.log("Connected to Mongo DB");
}).catch(err => {
    console.log("Error in connection");
})
module.exports = db;