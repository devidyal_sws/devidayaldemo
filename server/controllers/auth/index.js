//const { User } = require(__basedir + '/models');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

module.exports = {

  async login(req, res, next) {

    let username = req.body.username || '';
    let password = req.body.password || '';
    if (username == '' || password == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Invalid credentials"
      });
    } else {
      try {
        if(username ==='devidayal' && password ==='justdoit'){
          var token = jwt.sign({ uuid: username }, require(__basedir + '/helpers/secret')(), {
            expiresIn: 86400 // expires in 24 hours
          });
  
          res.json({
            "status": 200,
            "token": token,
            "auth" : true
          });

        }else{
          res.status(401);
          res.json({
            "status": 401,
            "message": "Invalid credentials"
          });
        }
      
      }catch (err) {
        res.json(err);
      }
    }
  }

};

