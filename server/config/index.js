module.exports = {
    db:{
        database:process.env.database || "oneb",
        user:process.env.dbuser || "root",
        password:process.env.dbpassword || "",
        options:{
            host: process.env.dbhost || "127.0.0.1",
            dialect: 'mysql'
        }
    },

}