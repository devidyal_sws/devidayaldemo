// Import the dependencies for testing
var chai = require('chai'); 
var chaiHttp = require('chai-http'); 
var app = 'http://localhost:3000';
// Configure chai
chai.use(chaiHttp);
chai.should();

describe("Tokenverification", () => {
    describe("POST /", () => {
        // Test to verify the JWT empty
        it("should get 403 if token is not passed in header", (done) => {
             chai.request(app)
                 .post('/api/v1/ip-management')
                 .send({ ipName: '111111111'})
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('object');
                     done();
                  });
         });
        // Test to verify the JWT with its header
        it("should get an object if token passed in header", (done) => {
            chai.request(app)
                .post('/api/v1/ip-management')
                .headers({'x-access-token':'some_value'})
                .send({ipName: '111111111'})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                 });
        });

    });
});