// Import the dependencies for testing
var chai = require('chai'); 
var chaiHttp = require('chai-http'); 
var app = 'http://localhost:3000';
// Configure chai
chai.use(chaiHttp);
chai.should();

describe("Authentication", () => {
    describe("POST /", () => {
        // Test to auth by email and password
        it("should get an object and otp on email", (done) => {
             chai.request(app)
                 .post('/auth')
                 .send({ username: 'devidayal.kushwaha@benosoftware.com', password: '111111' })
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('object');
                     done();
                  });
         });
        // Test to verify the otp from email
        it("should get an object and token", (done) => {
            chai.request(app)
                .post('/auth/verify')
                .send({ useremail: 'devidayal.kushwaha@benosoftware.com', userotp: '504824' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                 });
        });

        // Test to verify the otp from email
        it("should not get an object and token", (done) => {
            chai.request(app)
                .post('/auth/verify')
                .send({ useremail: 'devidayal.kushwaha@benosoftware.com', userotp: '111111' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                 });
        });
    });
});